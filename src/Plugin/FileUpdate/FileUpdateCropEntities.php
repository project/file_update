<?php

namespace Drupal\file_update\Plugin\FileUpdate;

use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Extension\ModuleHandler;
use Drupal\Core\Logger\LoggerChannel;
use Drupal\Core\Messenger\Messenger;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation file_update plugins.
 *
 * @FileUpdate(
 *   id = "crop",
 *   label = @Translation("Crop entities"),
 *   description = @Translation("Updates crop entities refering to image files."),
 *   permission = "administer nodes",
 * )
 */
class FileUpdateCropEntities extends FileUpdateBase implements FileUpdateInterface, ContainerFactoryPluginInterface {

  /**
   * Drupal\Core\Entity\EntityTypeManager definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * Drupal\Core\Extension\ModuleHandler definition.
   *
   * @var \Drupal\Core\Extension\ModuleHandler
   */
  protected $moduleHandler;

  /**
   * Drupal\Core\Messenger\MessengerInterface definition.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Drupal\Core\Logger\LoggerChannelInterface definition.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $loggerChannel;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManager $entity_type_manager, ModuleHandler $module_handler, LoggerChannel $logger_channel, Messenger $messenger) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
    $this->moduleHandler = $module_handler;
    $this->loggerChannel = $logger_channel;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('module_handler'),
      $container->get('logger.channel.file_update'),
      $container->get('messenger')
    );
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function isRequired() {
    return $this->moduleHandler->moduleExists('crop')
      && $this->entityTypeManager->getDefinition('crop', FALSE);
  }

  /**
   * {@inheritdoc}
   */
  public function getUsage() {
    $collector = [];
    foreach ($this->getCropEntities() as $crop_entity) {
      $collector[] = "crop:{$crop_entity->id()}";
    }
    return $collector;
  }

  /**
   * {@inheritdoc}
   */
  public function updateUri($uri) {
    if ($fid = $this->getFileId()) {
      foreach ($this->getCropEntities() as $corp_entity) {
        if ($uri != $corp_entity->get('uri')->getString()) {
          $corp_entity->set('uri', $uri);
          try {
            $corp_entity->save();
          }
          catch (EntityStorageException $e) {
            $this->messenger->addError($e->getMessage());
            $this->loggerChannel->error($e->getMessage());
          }
        }
      }
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function updateResolution($width, $height) {
    $fid = $this->getFileId();
    $cids = [];

    foreach ($this->getCropEntities() as $corp_entity) {
      $updated = FALSE;
      $cid = $corp_entity->id();
      $old_width = $this->getOldWidth();
      $old_height = $this->getOldHeight();

      // UPDATE DIMENSIONS if image was resized.
      if ($old_width && $old_height) {

        if (isset($width) && isset($height)
          && abs($old_width - $width) >= 1) {
          // Build redundant factors to build checksum.
          $factor_x = $width / $old_width;
          $factor_y = $height / $old_height;
          // Checksum is fine by max. < 2% rounding error tolerant.
          $checksum = (int) (round($factor_x * 50) - round($factor_y * 50));

          if ($checksum == 0) {
            // UPDATE POSITION.
            $position = $corp_entity->position();
            foreach ($position as $key => $cord) {
              $position[$key] = (int) round($cord * $factor_x);
            }
            if (isset($position['x']) && isset($position['y'])) {
              $corp_entity->setPosition($position['x'], $position['y']);
              $updated = TRUE;
            }

            // UPDATE SIZE.
            $size = $corp_entity->size();
            if ($size['width'] > 0 && $size['height'] > 0) {
              foreach ($size as $key => $cord) {
                $size[$key] = (int) round($cord * $factor_x);
              }
              if (isset($position['x']) && isset($position['y'])) {
                $corp_entity->setSize($size['width'], $size['height']);
                $updated = TRUE;
              }
            }
          }
          else {
            $message = $this->t('Did not update crop-entity (%cid). The xy-proprtions old (%old) and new (%new) does not match.', [
              '%cid' => $cid,
              '%old' => "{$old_width}x{$old_height}",
              '%new' => "{$width}x{$height}",
            ]);
            $this->messenger->addWarning($message);
            $this->loggerChannel->warning('Did not update crop-entity (%cid). The xy-proprtions old (%old) and new (%new) does not match.', [
              '%cid' => $cid,
              '%old' => "{$old_width}x{$old_height}",
              '%new' => "{$width}x{$height}",
            ]);
          }
        }
      }

      if ($updated) {
        $cids[] = $cid;
        if ($corp_entity->save() == SAVED_UPDATED) {
          $message = $this->t('Updated CropEntity %cid for old (%old) to new (%new) for FileEntity %fid.', [
            '%cid' => $cid,
            '%fid' => $fid,
            '%old' => "{$old_width}x{$old_height}",
            '%new' => "{$width}x{$height}",
          ]);
          $this->messenger->addMessage($message);
          $this->loggerChannel->info('Updated CropEntity %cid for old (%old) to new (%new) for FileEntity %fid.', [
            '%cid' => $cid,
            '%fid' => $fid,
            '%old' => "{$old_width}x{$old_height}",
            '%new' => "{$width}x{$height}",
          ]);
        }
      }
      else {
        $this->messenger->addWarning($this->t('Nothing to update in crop entity %id with file %file old (%old) to  new (%new).', [
          '%cid' => $cid,
          '%fid' => $fid,
          '%old' => "{$old_width}x{$old_height}",
          '%new' => "{$width}x{$height}",
        ]));
      }
    }
    return $cids;
  }

  /**
   * Returns all crop entities using a file.
   *
   * @return \Drupal\crop\Entity\Crop[]|\Drupal\Core\Entity\EntityInterface[]
   *   Returns array with crop entities referring to file entity.
   */
  protected function getCropEntities() {
    if ($fid = $this->getFileId()) {
      try {
        if ($this->isRequired()) {
          /** @var \Drupal\crop\Entity\Crop[] $corp_entities */
          return $this->entityTypeManager->getStorage('crop')
            ->loadByProperties(['entity_id' => $fid]);
        }
      }
      catch (\Exception $e) {
        $this->messenger->addError($e->getMessage());
        $this->loggerChannel->error($e->getMessage());
      }
    }
    return [];
  }

}
