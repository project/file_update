<?php

namespace Drupal\file_update\Plugin\FileUpdate;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Plugin type manager for file_update_field actions.
 *
 * @ingroup file_update
 */
class FileUpdateManager extends DefaultPluginManager {

  /**
   * {@inheritdoc}
   */
  public function __construct(
    \Traversable $namespaces,
    CacheBackendInterface $cache_backend,
    ModuleHandlerInterface $module_handler,
    $plugin_interface = 'Drupal\file_update\Plugin\FileUpdate\FileUpdateInterface',
    $plugin_definition_annotation_name = 'Drupal\file_update\Plugin\FileUpdate\FileUpdate'
  ) {
    parent::__construct('Plugin/FileUpdate', $namespaces, $module_handler, $plugin_interface, $plugin_definition_annotation_name);
  }

}
