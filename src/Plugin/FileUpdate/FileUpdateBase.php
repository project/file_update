<?php

namespace Drupal\file_update\Plugin\FileUpdate;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\file\FileInterface;

/**
 * Base class for FileUpdate-Plugins.
 */
abstract class FileUpdateBase {

  use StringTranslationTrait;

  /**
   * Array with plugin config (file_entity: the entity itself, file: props).
   *
   * @var array
   */
  protected $configuration;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    $this->setConfiguration($configuration);
  }

  /**
   * Returns references (usages) of this file from fields.
   *
   * @param string $field_type
   *   File type (optional).
   *
   * @return array
   *   Array with file references.
   */
  protected function getFileReference($field_type = 'file') {
    if ($file = $this->getFileEntity()) {
      return file_get_file_references($file, NULL, EntityStorageInterface::FIELD_LOAD_REVISION, $field_type);
    }
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration() {
    return $this->configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration) {
    $this->configuration = $configuration;
  }

  /**
   * Get current file entity.
   *
   * @return \Drupal\file\FileInterface|false
   *   The file entity.
   */
  protected function getFileEntity() {
    if (isset($this->configuration['file_entity']) && $this->configuration['file_entity'] instanceof FileInterface) {
      return $this->configuration['file_entity'];
    }
    return FALSE;
  }

  /**
   * Get id of current file entity.
   *
   * @return int|false
   *   Returns current file ID or false if no id is set.
   */
  protected function getFileId() {
    if (isset($this->configuration['file']['id']) && $this->configuration['file']['id']) {
      return (int) $this->configuration['file']['id'];
    }
    return FALSE;
  }

  /**
   * Get the old image width before validated file size.
   *
   * @return int
   *   The old height.
   */
  protected function getOldWidth() {
    return isset($this->configuration['file']['width'])
      ? (int) $this->configuration['file']['width']
      : 0;
  }

  /**
   * Get the old image height before validated file size.
   *
   * @return int
   *   The old height.
   */
  protected function getOldHeight() {
    return isset($this->configuration['file']['height'])
      ? (int) $this->configuration['file']['height']
      : 0;
  }

}
