<?php

namespace Drupal\file_update\Plugin\FileUpdate;

use Drupal\Core\Extension\ModuleHandler;
use Drupal\Core\Logger\LoggerChannel;
use Drupal\Core\Messenger\Messenger;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation file_update plugins.
 *
 * @FileUpdate(
 *   id = "image",
 *   label = @Translation("Image field type"),
 *   description = @Translation("Updates fields of type image (core)."),
 *   permission = "administer nodes",
 * )
 */
class FileUpdateImageField extends FileUpdateBase implements FileUpdateInterface, ContainerFactoryPluginInterface {

  /**
   * Drupal\Core\Extension\ModuleHandler definition.
   *
   * @var \Drupal\Core\Extension\ModuleHandler
   */
  protected $moduleHandler;

  /**
   * Drupal\Core\Messenger\MessengerInterface definition.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Drupal\Core\Logger\LoggerChannelInterface definition.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $loggerChannel;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ModuleHandler $module_handler, LoggerChannel $logger_channel, Messenger $messenger) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->moduleHandler = $module_handler;
    $this->loggerChannel = $logger_channel;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('module_handler'),
      $container->get('logger.channel.file_update'),
      $container->get('messenger')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function isRequired() {
    return $this->moduleHandler->moduleExists('image');
  }

  /**
   * {@inheritdoc}
   */
  public function getUsage() {
    $collector = [];
    foreach ($this->getFileReference('image') as $key => $usage) {
      foreach ($usage as $entity_type => $entities) {
        foreach ($entities as $entity_id => $entity) {
          $collector[] = "$entity_type:$entity_id->$key";
        }
      }
    }
    return $collector;
  }

  /**
   * {@inheritdoc}
   */
  public function updateUri($uri) {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function updateResolution($width, $height) {
    if (!(int) $width || !(int) $height) {
      return FALSE;
    }
    $fid = $this->getFileId();

    foreach ($this->getFileReference('image') as $field_name => $entities_array) {
      foreach ($entities_array as $entity_type_id => $entities) {
        /** @var \Drupal\Core\Entity\FieldableEntityInterface $entities */
        foreach ($entities as $entity_id => $entity) {
          $value = $entity->get($field_name)->getValue();
          foreach ($value as &$item) {
            if ($item['target_id'] == $fid) {
              $item['width'] = $width ?: $item['width'];
              $item['height'] = $height ?: $item['height'];
            }
          }
          $entity->set($field_name, $value);

          $link = ($entity->hasLinkTemplate('canonical')) ? $entity->toLink() : '';
          try {
            if ($entity->save() == SAVED_UPDATED) {
              $message = $this->t('Updated %type:%id %link entity at field %field with new image size %size.', [
                '%type' => $entity->getEntityTypeId(),
                '%id' => $entity->id(),
                '%link' => $link,
                '%field' => $field_name,
                '%size' => "{$width}x{$height}",
              ]);
              $this->messenger->addMessage($message);
              $this->loggerChannel->info('Updated %type:%id %link entity at field %field with new image size %size.', [
                '%type' => $entity->getEntityTypeId(),
                '%id' => $entity->id(),
                '%link' => $link,
                '%field' => $field_name,
                '%size' => "{$width}x{$height}",
              ]);
            }
          }
          catch (\Exception $exception) {
            $this->messenger->addError($exception->getMessage());
            $this->loggerChannel->error($exception->getMessage());
          }
        }
      }
    }
    return TRUE;
  }

}
