<?php

namespace Drupal\file_update\Plugin\FileUpdate;

use Drupal\Component\Utility\Html;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Site\Settings;
use Drupal\file\FileUsage\FileUsageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation file_update plugins.
 *
 * @FileUpdate(
 *   id = "editor",
 *   label = @Translation("File reference from editor fields."),
 *   description = @Translation("Updates fields of type editor (core)."),
 *   permission = "administer nodes",
 * )
 */
class FileUpdateEditor extends FileUpdateBase implements FileUpdateInterface, ContainerFactoryPluginInterface {

  /**
   * Drupal\Core\Entity\EntityTypeManager definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Drupal\Core\Extension\ModuleHandler definition.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Drupal\Core\Messenger\MessengerInterface definition.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Drupal\Core\Logger\LoggerChannelInterface definition.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $loggerChannel;

  /**
   * Drupal\file\FileUsage\DatabaseFileUsageBackend definition.
   *
   * @var \Drupal\file\FileUsage\FileUsageInterface
   */
  protected $fileUsage;

  /**
   * List of supported image styles.
   *
   * @var \Drupal\image\ImageStyleInterface[]
   */
  protected $imageStyles;

  /**
   * The image style paths.
   *
   * @var array
   */
  protected $imageStylePaths;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager,
    ModuleHandlerInterface $module_handler,
    LoggerChannelInterface $logger_channel,
    MessengerInterface $messenger,
    FileUsageInterface $file_usage
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
    $this->moduleHandler = $module_handler;
    $this->loggerChannel = $logger_channel;
    $this->messenger = $messenger;
    $this->fileUsage = $file_usage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('module_handler'),
      $container->get('logger.channel.file_update'),
      $container->get('messenger'),
      $container->get('file.usage')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function isRequired() {
    return $this->moduleHandler->moduleExists('editor');
  }

  /**
   * {@inheritdoc}
   */
  public function getUsage() {
    $collector = [];
    foreach ($this->getReferrer() as $key => $usage) {
      foreach ($usage as $entity_type => $entities) {
        foreach ($entities as $entity_id => $entity) {
          $collector[] = "$entity_type:$entity_id->$key";
        }
      }
    }
    return $collector;
  }

  /**
   * {@inheritdoc}
   */
  public function updateUri($uri) {
    $uuid = $this->getFileEntity()->uuid();
    $new_url = file_url_transform_relative(file_create_url($uri));
    $previous_uri = $this->configuration['file']['uri'];
    $previous_url = file_url_transform_relative(file_create_url($previous_uri));

    try {
      foreach ($this->getReferrer() as $field_name => $usage) {
        foreach ($usage as $entity_type => $entity_ids) {
          $entities = $this->entityTypeManager->getStorage($entity_type)
            ->loadMultiple($entity_ids);
          foreach ($entities as $entity_id => $entity) {
            if ($entity instanceof FieldableEntityInterface) {
              $context = [
                'entity' => $entity,
                'entity_type' => $entity_type,
                'entity_id' => $entity_id,
                'field_name' => $field_name,

                'uuid' => $uuid,
                'previous_url' => $previous_url,
                'uri' => $uri,
                'previous_uri' => $previous_uri,
                'new_url' => $new_url,
              ];
              $field_item = $entity->get($field_name);
              $field_value = $this->processFieldValue($field_item, $context);
              $field_item->setValue($field_value);

              if ($entity instanceof RevisionLogInterface) {
                // Add a revision message stating why the entity has changed.
                $entity->setRevisionLogMessage("file_update editor ($previous_uri) reference update.");
              }
              if ($entity->save() == SAVED_UPDATED) {
                $params = [
                  '%type' => $entity_type,
                  '%id' => $entity_id,
                  '%field' => $field_name,
                  '%url' => $new_url,
                ];
                $message = $this->t('Updated %type:%id entity at field %field with new image url "%url".', $params);
                $this->messenger->addMessage($message);
                $this->loggerChannel->info('Updated %type:%id entity at field %field with new image url "%url".', $params);
              }
            }
          }
        }
      }
    }
    catch (\Exception $e) {
      $this->messenger->addError($e->getMessage());
      $this->loggerChannel->error($e->getMessage());
    }
  }

  /**
   * Process the entity field values.
   */
  protected function processFieldValue(FieldItemListInterface $field_item, array $context) {
    $uuid = $context['uuid'];

    $values = $field_item->getValue();
    foreach ($values as $key => $value) {
      $properties = ['value'];
      if ($field_item->getFieldDefinition()->getType() === 'text_with_summary') {
        // Include the summary field.
        $properties[] = 'summary';
      }
      foreach ($properties as $property) {
        if (!empty($value[$property])) {
          // Normalize the line endings before loading.
          // https://www.drupal.org/project/drupal/issues/2975602
          $html = str_replace(["\r\n", "\r"], "\n", $value[$property]);
          $dom = Html::load($html);
          $xpath = new \DOMXPath($dom);
          foreach ($xpath->query('//*[@data-entity-type="file" and @data-entity-uuid="' . $uuid . '"]') as $node) {
            $this->processDomNode($node, $context);
          }
          $new_value = Html::serialize($dom);
          $values[$key][$property] = $new_value;
        }
      }
    }

    return $values;
  }

  /**
   * Process a DOM node element.
   */
  protected function processDomNode($node, array $context) {
    $previous_url = $context['previous_url'];
    $uri = $context['uri'];
    $previous_uri = $context['previous_uri'];
    $new_url = $context['new_url'];

    // Additional attributes that may be processed.
    $additional_attributes = Settings::get('file_update.editor_attributes', []);
    $attributes = array_unique(array_merge(['src'], $additional_attributes));
    foreach ($attributes as $attribute) {
      if ($attribute === 'srcset') {
        // This will be handled specially below.
        continue;
      }
      // Process the "src" attribute among others.
      /** @var \DOMElement $node */
      if ($node->hasAttribute($attribute)) {
        $src = $node->getAttribute($attribute);
        $new_src = $this->getUpdatedSrc(
          $src,
          $previous_url,
          $uri,
          $previous_uri,
          $new_url,
          ['attribute' => $attribute] + $context
        );
        // Update the attribute.
        $node->setAttribute($attribute, $new_src);
      }
    }

    if ($node->hasAttribute('srcset')) {
      // Update the srcset attribute specially if it exists.
      /** @see \Drupal\Component\Utility\Html::transformRootRelativeUrlsToAbsolute */
      $srcset = explode(',', $node->getAttribute('srcset'));
      $srcset = array_map('trim', $srcset);
      foreach ($srcset as $i => $srcset_entry) {
        $srcset_entry_components = explode(' ', $srcset_entry);
        // Apply the updated src.
        $new_srcset_url = $this->getUpdatedSrc(
          $srcset_entry_components[0],
          $previous_url,
          $uri,
          $previous_uri,
          $new_url,
          ['attribute' => 'srcset'] + $context
        );
        $srcset_entry_components[0] = $new_srcset_url;
        // Apply the updated components if it's changed.
        $srcset[$i] = implode(' ', $srcset_entry_components);
      }
      $node->setAttribute('srcset', implode(', ', $srcset));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function updateResolution($width, $height) {
    return TRUE;
  }

  /**
   * Returns all crop entities using a file.
   *
   * @return array
   *   Array with field items referring to current file entity.
   */
  protected function getReferrer() {
    $collector = [];
    try {
      if ($this->isRequired()) {
        $uuid = $this->getFileEntity()->uuid();
        foreach ($this->fileUsage->listUsage($this->getFileEntity()) as $key => $usage) {
          if ($key != 'editor') {
            continue;
          }
          foreach ($usage as $entity_type => $references) {
            $entities = $this->entityTypeManager->getStorage($entity_type)
              ->loadMultiple(array_keys($references));
            foreach ($entities as $entity) {
              if ($entity instanceof FieldableEntityInterface) {
                if ($field_uuids = _editor_get_file_uuids_by_field($entity)) {
                  $entity_id = $entity->id();
                  foreach ($field_uuids as $field_name => $uuids) {
                    if (in_array($uuid, $uuids, TRUE)) {
                      $collector[$field_name][$entity_type][$entity_id] = $entity_id;
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
    catch (\Exception $e) {
      $this->messenger->addError($e->getMessage());
      $this->loggerChannel->error($e->getMessage());
    }
    return $collector;
  }

  /**
   * Returns a list of image style entities.
   *
   * @return \Drupal\image\ImageStyleInterface[]
   *   The image styles.
   */
  protected function getImageStyles() {
    if ($this->imageStyles === NULL) {
      if ($this->moduleHandler->moduleExists('image')) {
        // Load the image styles.
        $this->imageStyles = $this->entityTypeManager
          ->getStorage('image_style')
          ->loadMultiple();
      }
      else {
        // Nothing to load.
        $this->imageStyles = [];
      }
    }

    return $this->imageStyles;
  }

  /**
   * Returns a list of image style path prefixes.
   */
  protected function getImageStylePaths() {
    if ($this->imageStylePaths === NULL) {
      $this->imageStylePaths = [];
      foreach ($this->getImageStyles() as $image_style) {
        $uri = $image_style->buildUri('');
        $uri = file_url_transform_relative(file_create_url($uri));
        $this->imageStylePaths[$image_style->id()] = rtrim($uri, '/') . '/';
      }
    }

    return $this->imageStylePaths;
  }

  /**
   * Retrieves the appropriate URI for the given src.
   *
   * @return string
   *   The URI.
   */
  protected function getUpdatedSrc($current_src, $previous_src, $new_uri, $previous_uri, $new_src, $context) {
    if (strpos($current_src, $previous_src) !== FALSE) {
      // URI matches the src given the previous name.
      return $new_src;
    }

    if (strpos($current_src, $previous_src) === FALSE) {
      // Doesn't match the original URL, check for image styles.
      foreach ($this->getImageStylePaths() as $style_id => $path_prefix) {
        if (strpos($current_src, $path_prefix) !== FALSE) {
          // Found a matching image style path. Try to recreate it with the new
          // file.
          $image_style = $this->getImageStyles()[$style_id] ?? NULL;
          if ($image_style && $image_style->supportsUri($new_uri)) {
            $new_style_derivative_url = $image_style->buildUrl($new_uri);
            return file_url_transform_relative($new_style_derivative_url);
          }
        }
      }
    }

    $params = [
      '%src' => $current_src,
      '%previous_src' => $previous_src,
      '%attribute' => $context['attribute'],
      '%type' => $context['entity_type'],
      '%id' => $context['entity_id'],
      '%field' => $context['field_name'],
    ];

    // Didn't match the expected value. Log it for debugging purposes.
    $this->loggerChannel->info(
      'The %attribute attribute had an unexpected URL value of "%src" on %type:%id entity at field %field. Expected it to contain "%previous_src".',
      $params
    );

    // Fallback to the new URL.
    return $new_src;
  }

}
