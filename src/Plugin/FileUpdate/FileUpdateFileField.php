<?php

namespace Drupal\file_update\Plugin\FileUpdate;

/**
 * Plugin implementation file_update plugins.
 *
 * @FileUpdate(
 *   id = "file",
 *   label = @Translation("File reference field type"),
 *   description = @Translation("Updates fields of type file (core)."),
 *   permission = "administer nodes",
 * )
 */
class FileUpdateFileField extends FileUpdateBase implements FileUpdateInterface {

  /**
   * {@inheritdoc}
   */
  public function isRequired() {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function getUsage() {
    $collector = [];
    foreach ($this->getFileReference() as $key => $usage) {
      foreach ($usage as $entity_type => $entities) {
        foreach ($entities as $entity_id => $entity) {
          $collector[] = "$entity_type:$entity_id->$key";
        }
      }
    }
    return $collector;
  }

  /**
   * {@inheritdoc}
   */
  public function updateUri($uri) {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function updateResolution($width, $height) {
    return TRUE;
  }

}
