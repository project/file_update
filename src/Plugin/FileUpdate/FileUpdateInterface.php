<?php

namespace Drupal\file_update\Plugin\FileUpdate;

/**
 * Provides an interface for defining Cronpub entity entities.
 *
 * @ingroup file_update
 */
interface FileUpdateInterface {

  /**
   * Check if module is installed and update can be processed.
   *
   * @return bool
   *   If resource update is required or not.
   */
  public function isRequired();

  /**
   * Check if module is installed and update can be processed.
   *
   * @return array|bool
   *   Array of usages of current type.
   */
  public function getUsage();

  /**
   * Action to execute on date start.
   *
   * @param string $uri
   *   The new uri to update managed items/entities.
   *
   * @return bool
   *   If resource update was successful.
   */
  public function updateUri($uri);

  /**
   * Update image resolution.
   *
   * @param int $width
   *   New image width.
   * @param int $height
   *   New image height.
   *
   * @return bool
   *   If resource update was successful.
   */
  public function updateResolution($width, $height);

}
