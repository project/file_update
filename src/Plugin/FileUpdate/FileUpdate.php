<?php

namespace Drupal\file_update\Plugin\FileUpdate;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a CronpubAction annotation object.
 *
 * A CronpubAction handles the actions of the cronpub module on an entity.
 * They are invoced by the CronpubExecutionService during a cronjob.
 *
 * @Annotation
 *
 * @see \Drupal\file_update\Plugin\FileUpdate\FileUpdateManager
 *
 * @ingroup file_update
 */
class FileUpdate extends Plugin {

  /**
   * The plugin ID representing field type.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the cronpub action.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * A short description of the cronpub action.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $description;

}
