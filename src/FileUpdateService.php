<?php

namespace Drupal\file_update;

use Drupal\Core\Entity\EntityMalformedException;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Image\ImageFactory;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Site\Settings;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\file\FileInterface;
use Drupal\file_update\Plugin\FileUpdate\FileUpdateManager;
use Symfony\Component\Mime\MimeTypeGuesserInterface;

/**
 * Class FileUpdateService.
 */
class FileUpdateService {

  use StringTranslationTrait;

  /**
   * Drupal\Core\Session\AccountProxyInterface definition.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Drupal\Core\Logger\LoggerChannelInterface definition.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $loggerChannel;

  /**
   * Drupal\Core\Messenger\MessengerInterface definition.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Drupal\Core\Image\ImageFactory definition.
   *
   * @var \Drupal\Core\Image\ImageFactory
   */
  protected $imageFactory;

  /**
   * Drupal\file\FileStorage definition.
   *
   * @var \Drupal\file\FileStorageInterface
   */
  protected $fileStorage;

  /**
   * Drupal\Core\File\FileSystemInterface definition.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * Drupal\Core\Extension\ModuleHandlerInterface definition.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The MIME type guesser.
   *
   * @var \Symfony\Component\Mime\MimeTypeGuesserInterface|\Symfony\Component\HttpFoundation\File\MimeType\MimeTypeGuesserInterface
   */
  protected $mimeTypeGuesser;

  /**
   * Drupal\file_update\Plugin\FileUpdate\FileUpdateFieldManager definition.
   *
   * @var \Drupal\file_update\Plugin\FileUpdate\FileUpdateManager
   */
  protected $pluginManager;

  /**
   * File entity set on init service.
   *
   * @var \Drupal\file\FileInterface
   */
  protected $fileEntity;

  /**
   * File properties set on init service.
   *
   * @var array
   */
  protected $fileProps;

  /**
   * Drupal\file_update\Plugin\FileUpdate\FileUpdateInterface definition.
   *
   * @var \Drupal\file_update\Plugin\FileUpdate\FileUpdateInterface[]
   */
  protected $plugins;

  /**
   * Constructs a new FileUpdateService object.
   *
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   Current user service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager service.
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger_channel
   *   Logger channel service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   Messenger service.
   * @param \Drupal\Core\Image\ImageFactory $image_factory
   *   Image factory service.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   File system service.
   * @param \Drupal\file_update\Plugin\FileUpdate\FileUpdateManager $file_update_manager
   *   FileUpdate plugin manager.
   * @param \Symfony\Component\Mime\MimeTypeGuesserInterface|\Symfony\Component\HttpFoundation\File\MimeType\MimeTypeGuesserInterface $mime_type_guesser
   *   The MIME type guesser.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(
    AccountProxyInterface $current_user,
    EntityTypeManagerInterface $entity_type_manager,
    LoggerChannelInterface $logger_channel,
    MessengerInterface $messenger,
    ImageFactory $image_factory,
    FileSystemInterface $file_system,
    FileUpdateManager $file_update_manager,
    $mime_type_guesser
  ) {
    $this->currentUser = $current_user;
    $this->entityTypeManager = $entity_type_manager;
    $this->loggerChannel = $logger_channel;
    $this->messenger = $messenger;
    $this->imageFactory = $image_factory;
    $this->fileSystem = $file_system;
    $this->pluginManager = $file_update_manager;
    $this->mimeTypeGuesser = $mime_type_guesser;

    $this->fileStorage = $this->entityTypeManager->getStorage('file');
  }

  /**
   * Init serve for operations on specific file.
   *
   * @param \Drupal\file\FileInterface|int $file
   *   Current file entity to work on.
   *
   * @return bool
   *   Returns TRUE if operation was successful.
   */
  protected function initService($file) {
    if (!$this->currentUser->hasPermission('update file entities')) {
      return FALSE;
    }
    $file_entity = $this->getFile($file);
    if ($this->fileEntity
      && $file_entity instanceof FileInterface
      && $this->fileEntity->id() == $file_entity->id()
    ) {
      return TRUE;
    }
    elseif ($file_entity instanceof FileInterface) {
      $this->fileEntity = $file_entity;
      return $this->setFileProps() && $this->setPlugins();
    }
    else {
      $this->messenger->addWarning($this->t('FileUpdateService can not initialize.'));
      return FALSE;
    }
  }

  /**
   * Image file loader.
   *
   * @param int|FileInterface $file
   *   The file entity id.
   *
   * @return \Drupal\file\FileInterface|false
   *   Returns File entity or FALSE if $entity is not an image file entity.
   */
  protected function getFile($file) {
    if ($file instanceof FileInterface) {
      $file_entity = $file;
    }
    else {
      /** @var \Drupal\file\Entity\File $file_entity */
      $file_entity = $this->fileStorage->load($file);
      if (!$file_entity) {
        $this->messenger->addWarning($this->t('File id %id not exists.', ['%id' => $file]));
        return FALSE;
      }
    }
    return $file_entity;
  }

  /**
   * Get configured plugins.
   *
   * @return bool
   *   Returns TRUE if operation was successful.
   */
  protected function setPlugins() {
    try {
      $config = [
        'file_entity' => $this->fileEntity,
        'file' => $this->getFileProps($this->fileEntity),
      ];
      $collector = [];
      foreach ($this->pluginManager->getDefinitions() as $key => $plugin_def) {
        /** @var \Drupal\file_update\Plugin\FileUpdate\FileUpdateInterface $plugin */
        $collector[$key] = $this->pluginManager->createInstance($key, $config);
      }
      $this->plugins = $collector;
      if ($usage = $this->getFileUsage()) {
        $this->fileProps['usage'] = $usage;
      }
      return TRUE;
    }
    catch (\Exception $e) {
      $this->messenger->addError($e->getMessage());
    }
    return FALSE;
  }

  /**
   * Set file props to class attributes.
   *
   * @return bool
   *   Returns TRUE if operation was successful.
   */
  protected function setFileProps() {
    $data = [
      'id' => $this->fileEntity->id(),
      'uuid' => $this->fileEntity->uuid(),
      'name' => $this->fileEntity->getFilename(),
      'mime' => $this->fileEntity->getMimeType(),
      'uri' => $this->fileEntity->getFileUri(),
      'url' => file_create_url($this->fileEntity->getFileUri()),
      'usage' => '',
      'size' => format_size($this->fileEntity->getSize()),
      'created' => date('Y-m-d h:m', $this->fileEntity->getCreatedTime()),
    ];
    try {
      $data['owner'] = $this->fileEntity->getOwner()->toLink();
    }
    catch (EntityMalformedException $e) {
    }
    $image = $this->imageFactory->get($data['uri']);
    if ($image->isValid()) {
      $data['width'] = $image->getWidth();
      $data['height'] = $image->getHeight();
      $data['preview'] = [
        '#theme' => 'image_style',
        '#style_name' => 'medium',
        '#uri' => $data['uri'],
      ];
    }
    else {
      $data['preview'] = FALSE;
    }
    $this->fileProps = $data;
    return TRUE;
  }

  /**
   * Returns a collection of file props.
   *
   * @param \Drupal\file\FileInterface|int $file_entity
   *   The file entity or ID.
   *
   * @return array|false
   *   Array with file properties.
   */
  public function getFileProps($file_entity) {
    if ($this->initService($file_entity)) {
      return $this->fileProps;
    }
    return FALSE;
  }

  /**
   * Returns an overview of file usages that will be updated as a string.
   *
   * @return string
   *   Collected usage separated by comma.
   */
  protected function getFileUsage() {
    $collector = [];
    foreach ($this->plugins as $plugin) {
      $collector = array_merge($collector, $plugin->getUsage());
    }
    return implode(', ', $collector);
  }

  /**
   * Main function.
   *
   * @param \Drupal\file\FileInterface|int $file
   *   The old name of the image used as identifier.
   * @param array $options
   *   The alt text of the image.
   *   - move_to: string
   *       update filename, path/, path/filename or schemed uri for SEO.
   *   - validate_max_size: int 0 or 1
   *       update images larger then setting.
   *   - max_width: int
   *       the max resolution width.
   *   - max_height: int
   *       the max resolution height.
   *
   * @return bool
   *   Returns TRUE if operation was successful.
   */
  public function updateFile($file, array $options) {
    $this->resetFileEntity();
    if ($this->initService($file)) {
      // Move file.
      if (isset($options['move_to']) && $options['move_to']) {
        $this->moveFile($options['move_to']);
      }

      // Resize physical file.
      if (isset($options['validate_max_size'])) {
        $this->fileValidateMaxResolution($options['validate_max_size']);
      }
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Reset the file and file prop field.
   */
  protected function resetFileEntity() {
    $this->fileEntity = NULL;
    $this->fileProps = NULL;
  }

  /**
   * Returns the current file entity.
   *
   * @return \Drupal\file\FileInterface|null
   *   The current file entity.
   */
  public function getCurrentFileEntity() {
    return $this->fileEntity;
  }

  /**
   * Returns the current file props.
   *
   * @return array
   *   The current file props.
   */
  public function getCurrentFileProps() {
    return $this->fileProps;
  }

  /**
   * Validates max resolution if file is an image.
   *
   * @param string $max_res
   *   Can be an empty array. You can set specific
   *   'max_with' and 'max_height' here.
   *
   * @return bool
   *   If operation was successful.
   */
  protected function fileValidateMaxResolution($max_res) {
    if ($this->isValidMaxRes($max_res)) {
      $results = file_validate_image_resolution($this->fileEntity, $max_res);
      if (!count($results)) {
        try {
          if ($this->fileEntity->save() == SAVED_UPDATED) {
            $image = $this->imageFactory->get($this->fileEntity->getFileUri());
            $width = $image->getWidth();
            $height = $image->getHeight();

            foreach ($this->plugins as $plugin) {
              $plugin->updateResolution($width, $height);
            }
          }
        }
        catch (EntityStorageException $e) {
          $this->messenger->addError($e->getMessage());
          $this->loggerChannel->error($e->getMessage());
        }
      }
      else {
        foreach ($results as $result) {
          $this->messenger->addError($result);
          $this->loggerChannel->error($result);
        }
      }
    }
    return TRUE;
  }

  /**
   * Checks if the value has correct form of {width}x{height}.
   *
   * Width and height must be >=10 and <=99999.
   *
   * @param string $value
   *   String to check.
   *
   * @return bool
   *   If string is a valid max resolution.
   */
  public function isValidMaxRes($value) {
    return 1 == preg_match('/^[0-9]{2,5}x[0-9]{2,5}$/', $value);
  }

  /**
   * Move or update file entity.
   *
   * @param string $filename
   *   A file name, path, path/file_name, uri.
   *
   * @return string|bool
   *   The destination file, or false if the operation was unsuccessful.
   */
  protected function moveFile($filename) {
    $uri_new = $this->prepareNewUri($this->fileEntity, $filename);
    $uri_old = $this->fileEntity->getFileUri();
    $fid = $this->fileEntity->id();
    // Check new filename differs from old.
    if ($uri_new != $uri_old) {
      $dir = $this->prepareDirectory($uri_new);

      $move_mode = Settings::get('file_update.move_mode', 'rename');
      switch ($move_mode) {
        case 'error':
          $replace = FileSystemInterface::EXISTS_ERROR;
          break;

        case 'replace':
          // NOTE: YOU MOST LIKELY DO NOT want to enable this option.
          // Two file entities may not reference the same URI, so the entity
          // returned by "file_move" in this mode may be changed to a different
          // entity than the one passed in.
          // In the situation that it creates a new entity while matching
          // against an existing URI. It can lead to orphan entities, and the
          // plugins will act on the "new" entity instead leading to unexpected
          // behaviour.
          // @todo investigate if there's an easy way to apply a replace without
          //   losing reference to the existing uuid.
          /* $replace = FileSystemInterface::EXISTS_REPLACE; break; */
          throw new \InvalidArgumentException('The replace option is not supported at the moment.');

        default:
          // Recommended: always create a new file in order to be uniquely
          // identifiable.
          $replace = FileSystemInterface::EXISTS_RENAME;
          break;
      }
      if ($dir && $move_success = file_move($this->fileEntity, $uri_new, $replace)) {
        try {
          if ($move_success->save() == SAVED_UPDATED) {
            $this->fileEntity = $move_success;
            $destination = $move_success->getFileUri();
            if ($uri_new !== $destination) {
              // Given that it's moving using FileSystemInterface::EXISTS_RENAME
              // by default, the destination file is not guaranteed to be free.
              // So log if a different file is being used instead.
              $context = [
                '%id' => $fid,
                '%old' => $uri_old,
                '%new' => $uri_new,
                '%dest' => $destination,
              ];
              $this->messenger->addWarning($this->t('Moved image (%id): %old => %dest. %new already exists.', $context));
              $this->loggerChannel->warning('Moved image (%id): %old => %dest. %new already exists.', $context);
            }
            else {
              $this->messenger->addMessage($this->t('Moved image (%id): %old => %destination', [
                '%id' => $fid,
                '%old' => $uri_old,
                '%dest' => $destination,
              ]));
            }

            foreach ($this->plugins as $plugin) {
              $plugin->updateUri($destination);
            }

            return $destination;
          }
        }
        catch (\Exception $exception) {
          $this->messenger->addError($exception->getMessage());
          $this->loggerChannel->error($exception->getMessage());
        }
      }
      else {
        $message = $this->t('Could not move image (%id): %old => %new', [
          '%id' => $fid,
          '%old' => $uri_old,
          '%new' => $uri_new,
        ]);
        $this->messenger->addWarning($message);
        $this->loggerChannel->warning('Could not move image (%id): %old => %new', [
          '%id' => $fid,
          '%old' => $uri_old,
          '%new' => $uri_new,
        ]);
      }
    }
    return FALSE;
  }

  /**
   * Prepares a new directory for a file uri if not exists or is not writeable.
   *
   * @param string $uri
   *   The complete uri including file name.
   *
   * @return bool
   *   Successful or not.
   */
  protected function prepareDirectory($uri) {
    $path = explode('/', $uri);
    /* $file_name = */ array_pop($path);
    $path = implode('/', $path);
    $dir = $this->fileSystem->prepareDirectory($path, FileSystemInterface::CREATE_DIRECTORY);
    $write = $this->fileSystem->prepareDirectory($path, FileSystemInterface::MODIFY_PERMISSIONS);
    return ($dir && $write);
  }

  /**
   * Create the new uri for moving file from various kinds of strings.
   *
   * @param \Drupal\file\FileInterface|int $file
   *   The file entity working on.
   * @param string $filename
   *   The filename e.g., path or uri.
   *
   * @return string|false
   *   The new uri.
   *   ''                                 -> public://old/path/old_file_name.jpg
   *   new_file_name                      -> public://old/path/new_file_name.jpg
   *   new_file_name.jpg                  -> public://old/path/new_file_name.jpg
   *   new_file_name.gif                  -> public://old/path/new_file_name.jpg
   *   new/path/new_file_name.jpg         -> public://new/path/new_file_name.jpg
   *   new/path/                          -> public://new/path/old_file_name.jpg
   *   /new/path/                         -> public://new/path/old_file_name.jpg
   *   public://folder/new_file_name.jpg  -> public://folder/my_super_file.jpg
   *   private://folder/new_file_name.jpg -> private://folder/my_super_file.jpg
   */
  public function prepareNewUri($file, $filename = NULL) {
    /** @var \Drupal\file\FileInterface $file_entity */
    $file_entity = $this->getFile($file);
    if (!$file_entity) {
      return FALSE;
    }

    if (preg_match('/\.[a-zA-Z0-9]{2,7}$/', $filename) == 1) {
      // It's not just a path that ends with a "/".
      if ($this->mimeTypeGuesser instanceof MimeTypeGuesserInterface) {
        $mime_guess = $this->mimeTypeGuesser->guessMimeType($filename);
      }
      else {
        // https://www.drupal.org/node/3133341
        // Deprecated in D9.1.
        $mime_guess = $this->mimeTypeGuesser->guess($filename);
      }
      if ($mime_guess == $file_entity->getMimeType()) {
        $new_filename_frags = explode('.', $filename);
        $ending = array_pop($new_filename_frags);
      }
      else {
        $this->messenger->addError($this->t('File has not correct extension.'));
        return FALSE;
      }
    }

    if (!isset($ending)) {
      $old_path_frags = explode('.', $file_entity->getFileUri());
      $ending = array_pop($old_path_frags);
    }
    $ending = ".{$ending}";

    $new_path_frags = explode('/', $filename);
    $old_uri = $file_entity->getFileUri();
    // Analyse filename.
    if (!$filename) {
      // No file name is given.
      return $old_uri;
    }
    elseif (count($new_path_frags) == 1) {
      // Just a file name given without path. Use old path with new file name.
      $filename_frags = explode('.', $filename);
      $filename = reset($filename_frags);
      $frags = explode('/', $old_uri);
      // Remove original filename.
      array_pop($frags);
      $frags[] = $filename . $ending;
      return implode('/', $frags);
    }
    elseif (
      count($new_path_frags) > 2
    // Means double slash.
      && $new_path_frags[1] == ''
    // Means stream wrapper e.g. "public:".
      && 1 == preg_match('/^[a-z]{2,12}:$/', $new_path_frags[0])
    ) {
      // URI given with stream wrapper and optional path.
      $filename = array_pop($new_path_frags);
      if ($filename == '') {
        // Just a new path given, use old file name.
        $new_path_frags[] = $file_entity->getFilename();
      }
      else {
        $filename_frags = explode('.', $filename);
        $filename = reset($filename_frags);
        $new_path_frags[] = $filename . $ending;
      }
      return implode('/', $new_path_frags);
    }
    else {
      // Path given without stream wrapper.
      $filename = array_pop($new_path_frags);
      // Remove empty to avoid trailing slashes.
      $new_path_frags = array_filter($new_path_frags);
      if ($filename == '') {
        // Just a new path given, use old file name.
        $new_path_frags[] = $file_entity->getFilename();
      }
      else {
        $filename_frags = explode('.', $filename);
        $filename = reset($filename_frags);
        $new_path_frags[] = $filename . $ending;
      }
      $new_path = implode('/', $new_path_frags);
      return 'public://' . $new_path;
    }
  }

}
