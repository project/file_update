<?php

namespace Drupal\file_update\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class FileUpdateForm.
 */
class FileUpdateForm extends FormBase {

  /**
   * Drupal\file_update\FileUpdateService definition.
   *
   * @var \Drupal\file_update\FileUpdateService
   */
  protected $updateService;

  /**
   * Drupal\Core\File\FileSystem definition.
   *
   * @var \Drupal\Core\File\FileSystem
   */
  protected $fileSystem;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Drupal\Core\StreamWrapper\StreamWrapperManager definition.
   *
   * @var \Drupal\Core\StreamWrapper\StreamWrapperManager
   */
  protected $streamWrapperManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->fileSystem = $container->get('file_system');
    $instance->updateService = $container->get('file_update.update_service');
    $instance->streamWrapperManager = $container->get('stream_wrapper_manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'file_update_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['#prefix'] = '<div id="intuitive-search-ajax-wrapper">';
    $form['#suffix'] = '</div>';
    $form['file_id'] = [
      '#type' => 'entity_autocomplete',
      '#title' => $this->t('File'),
      '#description' => $this->t('Start typing filename'),
      '#weight' => '1',
      '#target_type' => 'file',
      '#ajax' => [
    // don't forget :: when calling a class method.
        'callback' => [$this, 'fileEnteredAjaxCallback'],
    // Or TRUE to prevent re-focusing on the triggering element.
        'disable-refocus' => FALSE,
        'event' => 'autocompleteclose',
    // This element is updated with this AJAX callback.
        'wrapper' => 'intuitive-search-ajax-wrapper',
        'progress' => [
          'type' => 'throbber',
          'message' => $this->t('Verifying entry...'),
        ],
      ],
    ];

    if ($fid = $form_state->getValue('file_id')) {
      $form['fieldset'] = [
        '#type' => 'fieldset',
        '#title' => t('Properties'),
        '#weight' => 2,
        '#collapsible' => TRUE,
        '#collapsed' => FALSE,
      ];

      // Get the text of the selected option.
      if ($file_props = $this->updateService->getFileProps($fid)) {
        $preview = $file_props['preview'] ?? FALSE;
        unset($file_props['preview']);
        $form['fieldset']['props'] = [
          '#theme' => 'file_update_properties',
          '#preview' => $preview,
          '#props' => $file_props,
          '#weight' => '2',
        ];

        $form['fieldset']['file_uri'] = [
          '#type' => 'textfield',
          '#title' => $this->t('Edit uri'),
          '#description' => $this->t('Edit the uri. Do not change the stream wrapper (e.g. "public://") if you don’t know exactly what you do.'),
          '#required' => TRUE,
          '#maxlength' => 256,
          '#size' => 60,
          '#weight' => '3',
        ];

        // Resize params fields.
        $mime_types = ['image/jpeg', 'image/png', 'image/gif'];
        if (in_array($file_props['mime'], $mime_types)) {
          $form['fieldset']['max_resolution'] = [
            '#type' => 'textfield',
            '#name' => 'max_resolution',
            '#title' => $this->t('Max. resolution'),
            '#description' => $this->t('Scales down the image size to the max. resolution. {WIDTH}x{HEIGHT}'),
            '#required' => FALSE,
            '#default_value' => '2560x2560',
            '#maxlength' => 16,
            '#size' => 60,
            '#weight' => '4',
          ];
          $form['fieldset']['resize_image'] = [
            '#type' => 'checkbox',
            '#title' => $this->t('Resize image to max. resolution.'),
            '#description' => $this->t('If uploaded file is to large this option will scale it down to max resolution.'),
            '#weight' => '5',
          ];
        }
      }

      // Finalize form.
      $form['submit'] = [
        '#type' => 'submit',
        '#value' => $this->t('Submit'),
        '#weight' => '50',
      ];
    }

    return $form;
  }

  /**
   * Callback for ajax form operation.
   *
   * @param array $form
   *   The form definition.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   *
   * @return array
   *   Returns updated form.
   */
  public function fileEnteredAjaxCallback(array &$form, FormStateInterface $form_state) {
    if ($fid = $form_state->getValue('file_id')) {
      // Get the text of the selected option.
      // $fid = $form['file_id']['#value'];.
      if ($file_props = $this->updateService->getFileProps($fid)) {
        $form['fieldset']['file_uri']['#value'] = $file_props['uri'];
      }
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $form_values = $form_state->getValues();
    foreach ($form_values as $key => $value) {
      switch ($key) {
        case 'file_uri':
          if (!$this->streamWrapperManager->isValidUri($value)) {
            $form_state->setErrorByName($key, $this->t('The entered URI is not valid.'));
          }
          $fid = $form_values['file_id'];
          if (isset($form_values['file_id']) && $value) {
            if ($new_uri = $this->updateService->prepareNewUri($fid, $value)) {
              if (file_exists($new_uri)) {
                // Avoid unnecessary duplicates/ filename appends.
                $form_state->setErrorByName($key, $this->t('The destination URI %destination already exists.', [
                  '%destination' => $new_uri,
                ]));
              }
            }
            else {
              $form_state->setErrorByName($key, $this->t('The entered URI is not valid.'));
            }
          }
          break;

        case 'max_resolution':
          if (!$this->updateService->isValidMaxRes($value)) {
            $form_state->setErrorByName($key, $this->t('Max. resolution must be a value in shape of "1600x800".'));
          }
          break;

        default:
      }

    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    if ($fid = (int) $form_state->getValue('file_id')) {
      $options = ['move_to' => $form_state->getValue('file_uri')];
      if ($form_state->getValue('resize_image')) {
        $options['validate_max_size'] = $form_state->getValue('max_resolution');
      }
      $this->updateService->updateFile($fid, $options);
    }
  }

}
