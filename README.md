INTRODUCTION
------------

## File Update
Make adjustments to managed files - and transfer adjustments to resources
using these files (image fields, crop entities and more). The module is
suitable for SEO optimization in order to subsequently adapt image names and
paths to the important keywords of the website or landing page.

For a full description of the module, visit the [project page][file_update].

To submit bug reports and feature suggestions, or track changes,
[use issue page on Drupal.org][file_update_issues]

## Benefits
* Adjust file names and paths subsequently.
* Adjust image max. resolution subsequently.


### All adjustments are transferred to the following resources:
* Image fields (includes [media entities][media])
* File fields
* Crop entities ([Crop API][crop_api])
* Text fields (file uploads in CKEditor editor)

## Additional features:
If you make usage of file entities in custom modules: This module provides a
plugin interface. So you can create plugins for your custom module that will
transfer required adjustments to your custom resources (fields, entities,
whatever you want).

The editor plugin also provides a setting for specifying custom attributes which
should be updated. This should rarely be needed, but you may do so by adding
the following to your local `settings.php` file:

```php
// Provide support for updating custom block attributes.
$settings['file_update.editor_attributes'] = ['data-full-url', 'data-link'];
```

INSTALLATION
------------
* Just download and install File Update as [described here](
https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules).
* Configure "Update file entities" permissions. Please note the warning.

WARNING
-------
Using this module can lead to conflicts with prior versions (revisions) of
entities. It is strongly recommended to back up database and files folder (e.g.
/sites/default/files) before using the module.

It is also recommended to permit file updates only to persons who are aware of
the risks.

USAGE
-----
1. In the menu you go to „Configuration > Media > File Update“
   (/admin/file_update/update-form).
2. Select file by beginning to type a file name and select a provided file
resource from drop down.
3. Adjust uri and max. resolution in the fields.

RECOMMENDED MODULES
-------------------
* [Media (in Core)][media]

MAINTAINERS
-----------

Current maintainer:
 * [Joachim Feltkamp (JFeltkamp)][jfeltkamp]

## Drupal 9:
Will be ported to D9 with its first stable release.

[jfeltkamp]: https://www.drupal.org/u/jfeltkamp
[media]: https://www.drupal.org/docs/8/core/modules/media
[crop_api]: https://www.drupal.org/project/crop
[file_update]: https://www.drupal.org/project/file_update
[file_update_issues]: https://www.drupal.org/project/issues/file_update
